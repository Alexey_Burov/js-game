;let __START__ = false;
(function () {

        const widthContent = '850';
        const heightContent = '600';
        const w = 350;


        let container = document.getElementById('content');
        //выравнивание по центру
        container.style.width = `${widthContent}px`;
        container.style.margin = '0 auto';

        //выстрелы компьютера
        let shots = [];
        //текущий корабль
        let current = 10;

        let fieldNum = 4;
        let vert = {};
        let hor = {};

        const liters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K'];
        let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        const svgNS = svg.namespaceURI;
        svg.setAttribute('width', widthContent);
        svg.setAttribute('height', heightContent);


        let goNextNode = function (n) {
            let node = this;

            try {
                for (let i = 0; i < n; ++i)
                    node = node.nextElementSibling;
            } catch (e) {
                if (e.name === 'TypeError') {
                    node = null;
                }
            }
            return node;
        };

        let goPreviousNode = function (n) {
            let node = this;

            try {
                for (let i = n; i > 0; --i)
                    node = node.previousElementSibling;
            } catch (e) {
                if (e.name === 'TypeError') {
                    node = null;
                }
            }
            return node;
        };

        function checkDiagonal() {
            let topLeft, topRight, botLeft, botRight;
            topLeft = goPreviousNode.call(this, 13);
            topRight = goNextNode.call(this, 11);
            botLeft = goPreviousNode.call(this, 11);
            botRight = goNextNode.call(this, 13);
            if ((topLeft && topLeft.attributes.class.value === 'ships') ||
                (topRight && topRight.attributes.class.value === 'ships') ||
                (botLeft && botLeft.attributes.class.value === 'ships') ||
                (botRight && botRight.attributes.class.value === 'ships')) {
                this.setAttribute('class', 'field');
                return false;
            }
            return true;
        }

        let moveturnUser = true;

        let Field = (function () {

            function Field(i, j, flag, id) {
                let rect = document.createElementNS(svgNS, 'rect');
                rect.setAttribute('x', String(w / 10 * i));
                rect.setAttribute('y', String(w / 10 * j));
                rect.setAttribute('width', String(w / 10));
                rect.setAttribute('height', String(w / 10));
                if (!id)
                    rect.setAttribute('id', `${flag} ${liters[i]} ${(j + 1)}`);
                else
                    rect.setAttribute('id', id);
                rect.setAttribute('class', 'field');
                rect.setAttribute('state', 'Empty');
                rect.setAttribute('acces', flag);
                rect.addEventListener('mousedown', function (evt) {
                    ///right
                    if (evt.buttons === 2)
                        return;

                    //wheel
                    if (evt.buttons === 4) {
                        rect.setAttribute('class', 'field');
                        return;
                    }

                    if (this.attributes.acces.value === 'Preview')
                        return;

                    if (this.attributes.acces.value === 'Alien') {
                        if (__START__ && moveturnUser) {
                            if (this.attributes.class.value  === 'hideships' || this.attributes.class.value  === 'hit') {
                                rect.setAttribute('class', 'hit');

                                checkIntegralityAlien(this.id);

                            } else {
                                rect.setAttribute('class', 'miss');

                                moveturnUser = false;
                                document.getElementById('message').innerHTML = 'Промах';
                                setTimeout(computer, 1000);
                            }
                        }

                    } else {
                        ///после начала игры ввод недоступен
                        if (__START__) {
                            alert('Игра началась, кликайте по полю противника');
                            return;
                        }

                        if (this.className.baseVal === 'lock' || this.className.baseVal === 'freeze') {

                            return;
                        }

                        if (this.className.baseVal === 'ships') {

                            return;
                        }

                        //ввод поля корабля
                        this.setAttribute('class', 'ships');
                        checkFieldInput.call(rect);
                    }

                }, false);

                //проверка при вводе
                function checkFieldInput() {

                    if (!checkDiagonal.call(this)) return;

                    hor[this.id.split(' ')[1]] = this.id.split(' ')[2];
                    vert[this.id.split(' ')[2]] = this.id.split(' ')[1];

                    // if (Object.keys(vert).length > 1) {
                    //     checkForward.call(this);
                    // }

                    document.getElementById(`${current} ${fieldNum}`).setAttribute('class', 'ships');
                    if (current === 10) document.getElementById('message').innerHTML = 'Ввод 4-х палубного';

                    fieldNum -= 1;

                    if (fieldNum === 0) {


                        if (!freeze()) return;

                        current -= 1;

                        if (current === 9) {

                            fieldNum = 3;
                        }
                        if (current === 8) {

                            fieldNum = 3;
                        }
                        if (current === 7) {
                            fieldNum = 2;
                        }
                        if (current === 6) {
                            fieldNum = 2;
                        }
                        if (current === 5) {
                            fieldNum = 2;
                        }
                        if (current === 4) {
                            fieldNum = 1;
                        }
                        if (current === 3) {
                            fieldNum = 1;
                        }
                        if (current === 2) {
                            fieldNum = 1;
                        }
                        if (current === 1) {
                            fieldNum = 1;
                        }

                        if (fieldNum)
                            document.getElementById('message').innerHTML = `Ввод ${fieldNum}-х палубного`;
                        else
                            document.getElementById('message').innerHTML = 'Начало игры';

                        if (current === 0) {
                            alert('Начало игры');
                            __START__ = true;
                        }

                    }
                }

                return rect;
            }

            return Field;
        })();

        let generateSots = () => shots.pop();

        let computer = () => {
            let id = generateSots();
            if (!id) {
                alert('У компьютера закончились ходы.\nКонец игры.');
                document.getElementById('message').innerHTML = 'У компьютера закончились ходы.';
                return;
            }

            if (document.getElementById(id).attributes.class.value === 'ships') {
                document.getElementById(id).setAttribute('class', 'hit');
                document.getElementById('message').innerHTML = 'Попадание у компьютера';
                setTimeout(computer, 1000);
                console.log("Попадание");
            } else {
                document.getElementById(id).setAttribute('class', 'miss');
                document.getElementById('message').innerHTML = 'Промах у компьютера';
                moveturnUser = true;
                console.log("Промах");
            }
        };

        //текст буквы и цифры
        let createText = (x, y, text, className, id) => {
            let txt = document.createElementNS(svgNS, 'text');
            txt.setAttribute('x', x);
            txt.setAttribute('y', y);
            if (id) txt.setAttribute('id', id);
            txt.textContent = text;
            txt.setAttribute('class', className);
            return txt;
        };

        let init = () => {

            createContainer(0, 0, w, w, 50, 50, 'Own');
            createContainer(0, 0, w, w, 450, 50, 'Alien');

            let g = document.createElementNS(svgNS, 'g');
            g.appendChild(new Field(1, 12, 'Preview', '10 1'));
            g.appendChild(new Field(2, 12, 'Preview', '10 2'));
            g.appendChild(new Field(3, 12, 'Preview', '10 3'));
            g.appendChild(new Field(4, 12, 'Preview', '10 4'));
            g.appendChild(new Field(1, 14, 'Preview', '9 1'));
            g.appendChild(new Field(2, 14, 'Preview', '9 2'));
            g.appendChild(new Field(3, 14, 'Preview', '9 3'));
            g.appendChild(new Field(5, 14, 'Preview', '8 1'));
            g.appendChild(new Field(6, 14, 'Preview', '8 2'));
            g.appendChild(new Field(7, 14, 'Preview', '8 3'));
            g.appendChild(new Field(1, 16, 'Preview', '7 1'));
            g.appendChild(new Field(2, 16, 'Preview', '7 2'));
            g.appendChild(new Field(4, 16, 'Preview', '6 1'));
            g.appendChild(new Field(5, 16, 'Preview', '6 2'));
            g.appendChild(new Field(7, 16, 'Preview', '5 1'));
            g.appendChild(new Field(8, 16, 'Preview', '5 2'));
            g.appendChild(new Field(6, 12, 'Preview', '4 1'));
            g.appendChild(new Field(8, 12, 'Preview', '3 1'));
            g.appendChild(new Field(10, 12, 'Preview', '2 1'));
            g.appendChild(new Field(10, 14, 'Preview', '1 1'));
            g.setAttribute('transform', 'translate(12,0)');

            svg.appendChild(createText(w * 1.3, w * 1.3, 'mess', 'text', 'message'));
            svg.appendChild(g);

            container.appendChild(svg);

            current = 10;
            fieldNum = 4;
            vert = {};
            hor = {};
            initAlien();
        };

        let midl = false;
        //соседи
        let fieldNeighbours = [];

        //проверка целостности чужих
        let checkIntegralityAlien = (id) => {

            document.getElementById('message').innerHTML = 'Убил';

            let top = null;
            let bot = null;
            let left = null;

            let right = null;

            if (id.split(' ')[2] !== 1)
                top = document.getElementById(id).previousElementSibling;
            if (id.split(' ')[2] !== 10)
                bot = document.getElementById(id).nextElementSibling;
            if (id.split(' ')[1] !== 'K')
                right = goNextNode.call(document.getElementById(id), 12);

            if (id.split(' ')[1] !== 'A')
                left = goPreviousNode.call(document.getElementById(id), 12);

            if ((top && top.attributes.class.value === 'hideships') &&
                (bot && bot.attributes.class.value === 'hideships')) {
                fieldNeighbours[0] = top;
                fieldNeighbours[1] = bot;
                midl = true;
            }
            if ((right && right.attributes.class.value === 'hideships') &&
                (left && left.attributes.class.value === 'hideships')) {
                fieldNeighbours[0] = right;
                fieldNeighbours[1] = left;
                midl = true;
                return;
            }
            if ((top && top.attributes.class.value === 'hideships') ||
                (bot && bot.attributes.class.value === 'hideships') ||
                (right && right.attributes.class.value === 'hideships') ||
                (left && left.attributes.class.value === 'hideships')) {
                document.getElementById('message').innerHTML = 'Ранил';
                return true;
            }
            if (midl) {
                if (fieldNeighbours[0].attributes.class.value === 'hideships' ||
                    fieldNeighbours[1].attributes.class.value === 'hideships') {
                    document.getElementById('message').innerHTML = 'Ранил';
                    return true;
                }

                if (fieldNeighbours[0].attributes.class.value !== 'hideships' &&
                    fieldNeighbours[1].attributes.class.value !== 'hideships') {
                    document.getElementById('message').innerHTML = 'Убил';
                    return true;
                }
            }
            return false;
        };

        //проверка целостности своих
        let checkIntegrality = (fieldList, flag) => {
            if (current < 5) return true;
            let k;
            let offset;
            if (flag === 'H') {
                k = Object.keys(fieldList).sort();
                offset = liters.indexOf(k[0]);
            }
            if (flag === 'V') {
                k = Object.keys(fieldList);
                for (l in k) {
                    k[l] = +k[l];
                }
                offset = k[0];
            }
            for (l in k) {
                if (flag === 'H' && k[l] !== liters[(+l + offset)]) {
                    svg.innerHTML = '';
                    init();
                    alert('не верный ввод H');
                    return false;
                }
                if (flag === 'V' && k[l] !== (+l + Number(offset))) {
                    svg.innerHTML = '';
                    init();
                    alert('не верный ввод V');
                    return false;
                }
            }
            return true;
        };

        //фиксируем при окончании ввода
        let freeze = () => {
            if (Object.keys(hor).length > 1) {
                if (!checkIntegrality(hor, 'H'))
                    return false;
            } else {
                if (!checkIntegrality(vert, 'V'))
                    return false;
            }
            let k;
            if ((k = Object.keys(vert)).length > 1 || (Object.keys(vert).length === 1 && Object.keys(hor).length === 1)) {

                if (k[0] !== '1')
                    document.getElementById(`Own ${vert[k[0]]} ${(+k[0] - 1)}`).setAttribute('class', 'lock');
                if (k[k.length - 1] !== '10')
                    document.getElementById(`Own ${vert[k[k.length - 1]]} ${(+k[k.length - 1] + 1)}`).setAttribute('class', 'lock');

                for (let l in  vert) {
                    let literR = liters[liters.indexOf(vert[l]) + 1];
                    let literL = liters[liters.indexOf(vert[l]) - 1];
                    if (literR)
                        document.getElementById(`Own ${literR} ${l}`).setAttribute('class', 'lock');
                    if (literL)
                        document.getElementById(`Own ${literL} ${l}`).setAttribute('class', 'lock');
                }
            }
            if ((k = Object.keys(hor)).length > 1) {
                if (k.sort()[0] !== 'A')
                    document.getElementById(`Own ${liters[liters.indexOf(k.sort()[0]) - 1]} ${hor[k[0]]}`).setAttribute('class', 'lock');
                if (k.sort()[k.length - 1] !== 'K')
                    document.getElementById(`Own ${liters[liters.indexOf(k.sort()[k.length - 1]) + 1]} ${hor[k[0]]}`).setAttribute('class', 'lock');
                for (let l in  hor) {
                    if (+hor[l] !== 1)
                        document.getElementById(`Own ${l} ${(+hor[l] - 1)}`).setAttribute('class', 'lock');
                    if (+hor[l] !== 10)
                        document.getElementById(`Own ${l} ${(+hor[l] + 1)}`).setAttribute('class', 'lock');
                }
            }
            //очистить
            hor = {};
            vert = {};
            return true;
        };

//контейнер одного поля
        let createContainer = (x, y, w, h, trnsx, trnsy, flag) => {
            let g = document.createElementNS(svgNS, 'g');
            g.setAttribute('transform', `translate(${trnsx},${trnsy})`);
            g.setAttribute('id', flag);
            for (let i = 0; i < 10; i++) {
                for (let j = 0; j < 10; j++) {
                    g.appendChild(new Field(i, j, flag));
                }
                g.appendChild(createText(w / 10 * i + 10, 0 - 10, liters[i], 'text'));
                g.appendChild(createText(-w / 10, h / 10 * (i + 1) - 10, i + 1, 'text'));
            }
            svg.appendChild(g);
        };

        //расстановка противника
        let initAlien = () => {
            let ships = {};
            ships[0] = [
                'Alien A 3', 'Alien B 3', 'Alien C 3', 'Alien D 3',
                'Alien F 3', 'Alien F 4', 'Alien F 5',
                'Alien I 1', 'Alien I 2', 'Alien I 3',
                'Alien B 6', 'Alien C 6',
                'Alien F 7', 'Alien G 7',
                'Alien I 9', 'Alien I 10',
                'Alien B 9',
                'Alien D 8',
                'Alien E 10',
                'Alien G 10',
            ];

            ships[1] = [
                'Alien K 1', 'Alien K 2', 'Alien K 3', 'Alien K 4',
                'Alien F 1', 'Alien G 1', 'Alien H 1',
                'Alien D 1', 'Alien D 2', 'Alien D 3',
                'Alien A 1', 'Alien A 2',
                'Alien B 8', 'Alien C 8',
                'Alien I 9', 'Alien K 9',
                'Alien G 4',
                'Alien D 6',
                'Alien F 6',
                'Alien G 10',
            ];

            ships[2] = [
                'Alien I 4', 'Alien I 5', 'Alien I 6', 'Alien I 7',
                'Alien G 9', 'Alien H 9', 'Alien I 9',
                'Alien C 10', 'Alien D 10', 'Alien E 10',
                'Alien A 2', 'Alien B 2',
                'Alien I 1', 'Alien K 1',
                'Alien B 5', 'Alien B 6',
                'Alien E 3',
                'Alien F 5',
                'Alien D 8',
                'Alien A 9',
            ];

            const r = Math.round(Math.random() * 2);
            for (let id in ships[0])
                document.getElementById(ships[r][id]).setAttribute('class', 'hideships');

            for (let j = 1; j < 8; j += 3) {
                for (let i = 0; i < 7; i += 3) {
                    shots.push(`Own ${liters[i]} ${(j + 1)}`);
                    shots.push(`Own ${liters[i + 1]} ${(j)}`);
                    shots.push(`Own ${liters[i + 2]} ${(j + 2)}`);
                }
            }
        };

        init();
    }
)
();